const setupEvents = require('./installers/setupEvents')
if (setupEvents.handleSquirrelEvent()) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

const electronApp = require('electron');

const {
    app,
    BrowserWindow,
    Menu
} = electronApp;
const ProtocolHandler = require('./ProtocolHandler');
const PROTOCOL_SCHEME = 'hackerearth';
const PROTOCOL_PARAMETER_KEY = 'url';

const defaultWindowsConfig = {
    contextIsolation: false,
    nodeIntegration: true,
    nodeIntegrationInWorker: true,
    webSecurity: false,

    // fullscreen: true,
    show: false,
    // alwaysOnTop: true,
    // maximizable: false,
    // minimizable: false,
    // movable: false,
    // resizable: false
};

const allowedKeysWithCtr = ['q', 'a', 'z', 'r', 'control', 'shift'];
const allowedKeysWithAlt = ['f4', 'alt'];
const notAllowedOtherKeys = ['f1', 'f2', 'f3', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12', 'MediaNextTrack', 'MediaPreviousTrack', 'MediaStop', 'MediaPlayPause', 'PrintScreen'];

// Making the user-agent static as of now, else we get error in System checks
const userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'

let mainWindow;
let DEFAULT_URI;
let PARAMS_URL;

// DEFAULT_URI = 'https://www.hackerearth.com/practice/';
DEFAULT_URI = 'https://p.hck.re/EFcD';

const protocolHandler = new ProtocolHandler(PROTOCOL_SCHEME, (linkUrl) => {
    if (mainWindow && mainWindow.webContents) {
        PARAMS_URL = linkUrl.searchParams.get(PROTOCOL_PARAMETER_KEY);
        return true;
    }
    return false;
});

// Force Single Instance Application
const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
    app.quit()
} else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
        // Someone tried to run a second instance, we should focus our window.
        if (mainWindow) {
            if (mainWindow.isMinimized()) mainWindow.restore()
            mainWindow.focus()
        }
    })
        
    // Create mainWindow, load the rest of the app, etc...
    app.on('ready', () => {
    })
}

// Create menu template
const mainMenuTemplate = [
    // Each object is a dropdown
    {
        label: 'File',
        submenu: [{
                role: 'quit'
            }
        ]
    },
    {
        label: 'Edit',
        submenu: [{
                role: 'undo'
            },
            {
                role: 'redo'
            },
            {
                type: 'separator'
            },
            {
                role: 'delete'
            },
            {
                type: 'separator'
            },
            {
                role: 'selectAll'
            }
        ]
    },
    {
        label: 'View',
        submenu: [{
                role: 'reload'
            },
            {
                role: 'forceReload'
            }
        ]
    }
];

mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [{
        label: 'Toggle DevTools',
        accelerator: 'Ctrl+I',
        click(item, focusedWindow) {
            focusedWindow.toggleDevTools();
        }
    }]
});

function createWindow() {
    mainWindow = new BrowserWindow(defaultWindowsConfig);

    protocolHandler.onAppReady();

    // Load a DEFAULT URL or PARAMS_URL is its coming from deeplink
    if (PARAMS_URL)
        mainWindow.loadURL(PARAMS_URL, {userAgent: userAgent});
    else
        mainWindow.loadURL(DEFAULT_URI, {userAgent: userAgent});
    mainWindow.maximize();
    // mainWindow.setContentProtection(true);
    mainWindow.show();

    // Open the DevTools.
    mainWindow.webContents.openDevTools()
    console.log(mainWindow.webContents.getUserAgent());

    // mainWindow.webContents.on('before-input-event', (event, input) => {
    //     if (input.control && !allowedKeysWithCtr.includes(input.key.toLowerCase())) {
    //         event.preventDefault();
    //     } else if (input.alt && !allowedKeysWithAlt.includes(input.key.toLowerCase())) {
    //         event.preventDefault();
    //     } else if (notAllowedOtherKeys.includes(input.key.toLowerCase())) {
    //         event.preventDefault();
    //     }
    // });

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    // Insert menu
    Menu.setApplicationMenu(mainMenu);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
