const installer = require('electron-installer-debian')
const path = require('path')

const rootPath = path.join('./')
const outPath = path.join(rootPath, 'release-builds')
const options = {
  src: path.join(outPath, 'HackerEarth-linux-x64'),
  dest: path.join(outPath, 'linux-installer'),
  arch: 'amd64',
  icon: path.join(rootPath, 'assets', 'linux', 'HE.png'),
  'options.productName': 'HackerEarth',
  'options.mimeType': 'x-scheme-handler/hackerearthapp'
}
 
async function main (options) {
  console.log('Creating package (this may take a while)')
  try {
    await installer(options)
    console.log(`Successfully created package at ${options.dest}`)
  } catch (err) {
    console.error(err, err.stack)
    process.exit(1)
  }
}
main(options)
